'use strict'

import jQuery from 'jquery';
import popper from 'popper.js';
import bootstrap from 'bootstrap';
import fontawesome from '@fortawesome/fontawesome-free/js/all'

import "./style.scss"

import Task from "./task"

$(document).ready(function () { //Выводим все задачи
    let template = $('.accordion').prop('content');
    var showAlltasks = function () {
        $('.tasks').find('.card').remove();
        let tasks = Task.getAllTasks();
        $.each(tasks, function (index, value) {
            let item = $(template).find('.card').clone(true);
            $(item).attr('data-id', value.id);
            $(item).find('.card-header').find('.btn-link.collapsed').attr('data-target', '#' + value.id).text(value.name);
            $(item).find('.card-body').text(value.description).closest('.collapse').attr('id', value.id);
            if (!value.done) {
                $(item).find('.task-done').removeClass('fa-check-circle text-success').addClass('fa-times-circle text-danger');
            }            
            $('.tasks').append(item);
        });
    }
    var validationForm = function (form) {
        let isValid = true;
        $(form).find('input,textarea').not('[type="hidden"],[type="checkbox"]').each(function (idx) {
            if ($(this).val() == '') {
                $(this).css('border', '1px solid red');
                isValid = false;
            } else {
                $(this).css('border', '');
            }
        });
        return isValid;
    }

    showAlltasks();

    $('.saveTask').on('click', function () {
        let form = $(this).closest('.modal-content').find('.task-form');
        if (!validationForm(form)) return;
        let name = $(form).find('.nameTask').val();
        let descrTask = $(form).find('.descrTask').val();
        let id = ($(form).find('.task-id').val() != "") ? $(form).find('.task-id').val() : null;
        let done = $(form).find('.toggle-switch input').is(':checked');
        let task = new Task(name, descrTask, done, id);
        task.saveTask();
        showAlltasks();
        $(form).closest('.modal').modal('hide');
        $(form).trigger('reset');
    });

    $('body').on('click', '.edit-task', function (e) {
        e.stopPropagation();
        let taskId = $(this).closest('.card').attr('data-id');
        let task = Task.getTask(taskId);
        if (task) {
            let form = $('#updateTaskModal').find('.task-form');
            $(form).find('.nameTask').val(task.name);
            $(form).find('.descrTask').val(task.description);
            $(form).find('.task-id').val(task.id);
            $(form).find('.toggle-switch input').prop('checked', task.done);
            $('#updateTaskModal').modal('show');
        }
    });

    $('body').on('click', '.delete-task', function (e) {
        e.stopPropagation();
        let task = $(this).closest('.card');
        let taskId = $(task).attr('data-id');
        Task.removeTask(taskId);
        $(task).remove();
    });
});