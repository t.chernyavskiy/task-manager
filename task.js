'use strict'

export default class Task {

    constructor(name, description, done, id = null) {
        this.name = name;
        this.description = description;
        this.done = done;
        this.id = id;
    }

    saveTask() {
        let tasks = Task.getAllTasks();
        if (this.id === null) { //Новая задача
            this.id = '_' + Math.random().toString(36).substr(2, 9);
        }
        tasks[this.id] = this;
        localStorage.setItem('Tasks', JSON.stringify(tasks));
    }

    static getTask(id) {
        let tasks = Task.getAllTasks();
        return (id in tasks) ? tasks[id] : false;
    }

    static getAllTasks() {
        return (localStorage.hasOwnProperty('Tasks')) ? JSON.parse(localStorage.getItem('Tasks')) : {};
    }

    static removeTask(id) {
        let tasks = Task.getAllTasks();
        if (id in tasks) {
            delete tasks[id];
            localStorage.setItem('Tasks', JSON.stringify(tasks));
        }
    }

    static clearAllTasks() {
        localStorage.removeItem('Tasks');
    }
}